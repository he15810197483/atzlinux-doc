导言
======

## 1 安装
- https://www.atzlinux.com/index.htm
- https://www.atzlinux.com/yjaz.htm
- https://www.atzlinux.com/allpackages.htm

## 2 基本操作

更详细的说明可参考 [debian 参考手册](https://www.atzlinux.com/doc/manuals/debian-reference/index.zh-cn.html)

### 软件包的安装

```
apt-get install _软件包的名字_
```

### 打开应用程序

对于大多数已经配置桌面图标的应用程序，双击桌面上的应用程序的图标即可。

对于非GUI的应用程序，通常在命令提示符中输入其名字既可以允许其相应的程序，例如:

```
gcc -h
```

## 3 普通用户
https://www.atzlinux.com/faq.htm

## 4 高级用户

https://www.atzlinux.com/skills.htm

## 5 贡献者

https://gitee.com/atzlinux/debian-cn/issues
https://www.atzlinux.com/juanzeng.htm

## 6 开发者

https://www.atzlinux.com/devel.htm

https://gitee.com/organizations/atzlinux/projects