# 《铜豌豆 Linux》手册目录

这里包括了一些面向新手的使用铜豌豆 Linux的经验、技巧、示例、知识等等方面的总结，对于已经有使用 Linux 操作系统经验的高级用户，选择感兴趣的章节阅读也是可以的。

欢迎贡献到这个文档的汇编、整理、优化中来。

[导言](intro/_intro.md)
[和朋友、同事、社群通讯](comm/_comm.md)
[编程](prog/_prog.md)
[网络和自动化](naa/_naa.md)
[艺术、出版和出品](app/_app.md)
[内核与高级linux开发](kern/_kern.md)
[成为铜豌豆开发者](contr/_contr.md)
